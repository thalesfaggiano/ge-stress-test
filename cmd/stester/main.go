package main
import (
	"gitlab.com/thalesfaggiano/ge-stress-test/internal/infrastructure"
)
func main() {
	container := infrastructure.NewContainer()
	cli := container.GetCLI()
	cli.Execute()
}
