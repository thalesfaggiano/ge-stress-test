# GE-Stress Test

- [GE-Stress-Test](#ge-stress-test)
  * [Motivação](#motivação)
  * [Desafio](#desafio)
  * [Documentação](#documentação)

## Motivação

Este é um desafio da Pós-Graduação da Fullcycle: Go Expert, Desenvolvimento Avançado em Golang

## Desafio

**Objetivo:**
Criar um sistema CLI em Go para realizar testes de carga em um serviço web. O usuário deverá fornecer a URL do serviço, o número total de requests e a quantidade de chamadas simultâneas.

O sistema deverá gerar um relatório com informações específicas após a execução dos testes.

**Entrada de Parâmetros via CLI:**

--url: URL do serviço a ser testado.
--requests: Número total de requests.
--concurrency: Número de chamadas simultâneas.

**Execução do Teste:**

Realizar requests HTTP para a URL especificada.
Distribuir os requests de acordo com o nível de concorrência definido.
Garantir que o número total de requests seja cumprido.

**Geração de Relatório:**

- Apresentar um relatório ao final dos testes contendo:
	- Tempo total gasto na execução
	- Quantidade total de requests realizados.
	- Quantidade de requests com status HTTP 200.
	- Distribuição de outros códigos de status HTTP (como 404, 500, etc.).

Execução da aplicação:
- Poderemos utilizar essa aplicação fazendo uma chamada via docker. Ex:
	- docker run <sua imagem docker> —url=http://google.com —requests=1000 —concurrency=10

## Documentação

O nome do nosso teste de estresse chama-se: stester

O stester é construido e Golang, na versão 1.22.3. E embora o Golang seja retrocompatível, todos os testes foram realizados apenas nesta versão.

Se você o executar por meio de um terminal, o Quickstart Bash foi realizado em sua versão: 5.1.16

E caso queira realizar o Quickstart Docker as versões utilizadas neste teste foram:
```
Server: Docker Engine - Community
 Engine:
  Version:          26.1.1
  OS/Arch:          linux/amd64
 containerd:
  Version:          1.6.31
 runc:
  Version:          1.1.12
 docker-init:
  Version:          0.19.0
```

Para os exemplos de execução nos Quickstart você deverá:

1 - Fazer download do código atualizado:

```bash
git clone git@gitlab.com:thalesfaggiano/ge-stress-test.git
```
E em seguida acessar seu diretório:

```bash
cd ge-stress-test
```

E siga os passos do Quickstart que preferir.

OBS.: Todos os exemplos estressam o site do **google.com** com **100 requests** e com **10 concorrências**. Basta mudar estes valores para adapta-lo a sua necessidade.

---
### Quickstart Bash

Passo 1:

```bash
go build -o stester ./cmd/stester
```

Passo 2:

```bash
./stester --url=http://google.com --requests=10 --concurrency=5
```

### Quickstart Docker

Passo 1:

```bash
docker build -t ge-stress-test .
```

Passo 2:

```bash
docker run ge-stress-test --url=http://google.com --requests=10 --concurrency=5
```

